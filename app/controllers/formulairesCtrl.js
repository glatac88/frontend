define(['app','noty'], function (app,noty) {
    app.register.controller('formulairesCtrl', ['$rootScope', '$scope','$timeout','$location', function ($rootScope, $scope,$timeout,$location) {

        //Initialisation variables
        $scope.titre = 'Ajout ' + $scope.itemList.titreFormAdd;
        $scope.isEdit = false;
        $scope.saveLoading = false;
        $scope.liste = [];
        $scope.errors = [];
        var id = $rootScope.currentRoute.params['id'];

        // //Add ou edit
        if(id != null) {
            //Récupération des infos
            axios.get('/'+$scope.itemList.modelNom+'/'+id)
                .then(function (response) {
                    $scope.$apply(function () {
                        $scope.liste = response.data.data;
                        $scope.isEdit = true;
                        $scope.id = id;

                        //Si le champ label s'appelle autrement
                        if ($scope.liste['label'] === undefined) {  //si non label
                            if ($scope.liste['nom'] === undefined) {    //si non nom
                                $scope.liste['label'] = $scope.liste['reference'];  //alors reference
                            } else {
                                if ($scope.liste['prenom'] !== undefined) {
                                    $scope.liste['label'] = $scope.liste['nom'] + ' ' + $scope.liste['prenom'];
                                } else {
                                    $scope.liste['label'] = $scope.liste['nom'];
                                }
                            }
                        }
                        $scope.titre = 'Modification ' + $scope.itemList.titreFormUpdate + ' | ' + $scope.liste['label'];

                        //En fonction des types
                        $scope.itemList.champs.forEach(function(champ) {
                            if (champ['type'] === "input_number")
                                $scope.liste[champ['field']] = parseInt($scope.liste[champ['field']]);
                            if (champ['type'] === "input_float")
                                $scope.liste[champ['field']] = parseFloat($scope.liste[champ['field']]);
                            if (champ['type'] === "input_cpville") {
                                $scope.liste[champ['field']['cp']] = $scope.liste[champ['field']['cp']];
                                $scope.liste[champ['field']['ville']] = $scope.liste[champ['field']['ville']];
                            }
                        });
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                    });
                });
        }

        /**
         * Création / Modification d'un élément
         * @param id
         */
        $scope.update = function (id = null) {
            var data = new FormData();

            //Récupération des paramètres
            $scope.itemList.champs.forEach(function(champ) {

                if (typeof champ.field === 'object') {

                    Object.keys(champ.field).map(function(objectKey, index) {

                        var value = champ.field[objectKey];
                        data.append(value, $scope.liste[value] ? $scope.liste[value] : '');
                    });

                } else {

                    data.append(champ['field'], $scope.liste[champ['field']] ? $scope.liste[champ['field']] : '');
                }
            });

            $scope.saveLoading = true;

            //Enregistrement
            if (id == null) //Ajout
            {
                axios.post('/'+$scope.itemList.modelNom,data)
                    .then(function () {
                        $scope.saveLoading = false;
                        $timeout(function () {
                            noty.showNotif('Élément créé', 'success');
                            $location.path("/"+$scope.itemList.moduleNom+"/"+$scope.itemList.dossierNom+"/liste-"+$scope.itemList.dossierNom);
                        });
                    })
                    .catch(function (error) {
                        $scope.saveLoading = false;
                        $scope.$apply(function () {
                            noty.showNotif('L\'élément n\'a pas pu être créé', 'error');

                            if (error.response && error.response.data && error.response.data.messages)
                                $scope.errors = error.response.data.messages;
                        });
                    });
            }
            else    //Modif
            {
                axios.post('/'+$scope.itemList.modelNom+'/'+id,data)
                    .then(function () {
                        $scope.saveLoading = false;
                        $timeout(function () {
                            noty.showNotif('Élément modifié', 'success');
                            $location.path("/"+$scope.itemList.moduleNom+"/"+$scope.itemList.dossierNom+"/liste-"+$scope.itemList.dossierNom);
                        });
                    })
                    .catch(function (error) {
                        $scope.saveLoading = false;
                        $scope.$apply(function () {

                            if (error.response && error.response.data && error.response.data.messages)
                                noty.showNotif('L\'élément n\'a pas pu être modifié : ' + error.response.data.messages.join(' | '), 'error');
                        });
                    });
            }
        };

        /**
         * Suppression d'un élément
         */
        $scope.delete = function () {
            axios.delete('/'+$scope.itemList.modelNom+'/'+id)
                .then(function (response) {
                    $timeout(function () {
                        noty.showNotif('Élément archivé', 'success');
                        $location.path("/"+$scope.itemList.moduleNom+"/"+$scope.itemList.dossierNom+"/liste-"+$scope.itemList.dossierNom);
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément n\'a pas pu être archivé', 'error');
                    });
                });
        };

        /**
         * Ouvre une modale générique
         * @param model
         */
        $scope.openGenericModal = function (model) {
            $('#modal'+ model + '').modal('show');
        };

        /**
         * Ajoute un élément à un tableau déjà présent dans un formulaire
         * @param model
         */
        $scope.addElemToTab = function (model) {
            var data = new FormData();

            //Récupération des paramètres de la modale
            $scope.itemList.champs.forEach(function(champ) {
                if (typeof champ.field === 'object' && champ.type === 'input_grid') {

                    Object.keys(champ.field).map(function(objectKey) {

                        var value = champ.field[objectKey];
                        data.append(value, $scope.liste[value] ? $scope.liste[value] : '');
                    });
                } else {
                    data.append(champ['field'], $scope.liste[champ['field']] ? $scope.liste[champ['field']] : '');
                }
            });


        }
    }])
});
