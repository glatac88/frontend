'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

// noinspection JSUnusedLocalSymbols
define(['app', 'formulairesCtrl'], function (app, formulairesCtrl) {

    /**
     * @memberof app
     * @ngdoc controllers
     * @name formulaireComposantCtrl
     */
    app.register.controller('formulaireComposantCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            var tvaOptions = [];
            var fournisseurOptions = [];
            var familleComposantOptions = [];

            axios.all([
                axios.get('/tva/search'),
                axios.get('/fournisseur/search'),
                axios.get('/famille-composant/search')
            ]).then(axios.spread(function (tRes, fRes, fcRes) {

                tRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        tvaOptions.push({
                            text: entity.label,
                            value: entity.id
                        });
                    });
                });

                fRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        fournisseurOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });

                fcRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        familleComposantOptions.push({
                            text: entity.label,
                            value: entity.id
                        });
                    });
                });
            }));

            // axios.get('/famille-composant/search')
            //     .then(function (response) {
            //
            //         response.data.data.map(function (entity) {
            //
            //             familleComposantOptions.push({
            //                 text: entity.label,
            //                 value: entity.id
            //             });
            //         });
            //     });

            $scope.itemList = [];
            $scope.itemList.modelNom = 'composant';
            $scope.itemList.moduleNom = 'settings';
            $scope.itemList.dossierNom = 'composants';
            $scope.itemList.titreFormAdd = "d'un composant";
            $scope.itemList.titreFormUpdate = 'du composant';
            $scope.itemList.champs = [
                {
                    nom: 'Nom *',
                    field: 'nom',
                    type: 'input_text',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Colisage *',
                    field: 'colisage',
                    type: 'input_number',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Prix HT *',
                    field: 'prix_ht',
                    type: 'input_float',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Marge *',
                    field: 'marge',
                    type: 'input_float',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Unité de colisage *',
                    field: 'unite_colisage',
                    type: 'input_text',
                    parentDivClass: 'col-12',
                    params: {
                        maxlength: 3
                    }
                },
                {
                    nom: 'Description *',
                    field: 'description',
                    type: 'input_textarea',
                    parentDivClass: 'col-12'
                },
                {
                    nom: 'Fournisseur *',
                    field: 'id_fournisseur',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    options: fournisseurOptions
                },
                {
                    nom: 'Famille de composant *',
                    field: 'id_famille_composant',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    options: familleComposantOptions
                },
                {
                    nom: 'Tva *',
                    field: 'id_tva',
                    type: 'input_select',
                    parentDivClass: 'col-12',
                    options: tvaOptions
                }
            ];

            angular.extend(this, $controller('formulairesCtrl', {$scope: $scope}));
        }]);
});
