'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

// noinspection JSUnusedLocalSymbols
define(['app', 'formulairesCtrl'], function (app, formulairesCtrl) {

    /**
     * @memberof app
     * @ngdoc controllers
     * @name formulaireModuleCtrl
     */
    app.register.controller('formulaireModuleCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            var GammeOptions = [];
            var NatureModuleOptions = [];
            var ComposantOptions = [];

            axios.all([
                axios.get('/gamme/search'),
                axios.get('/nature-module/search'),
                axios.get('/composant/search')
            ]).then(axios.spread(function (gRes, nmRes,cRes) {

                gRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        GammeOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });

                nmRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        NatureModuleOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });

                cRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        ComposantOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });
            }));

            // axios.get('/famille-composant/search')
            //     .then(function (response) {
            //
            //         response.data.data.map(function (entity) {
            //
            //             familleComposantOptions.push({
            //                 text: entity.label,
            //                 value: entity.id
            //             });
            //         });
            //     });

            $scope.itemList = [];
            $scope.itemList.modelNom = 'module';
            $scope.itemList.moduleNom = 'settings';
            $scope.itemList.dossierNom = 'modules';
            $scope.itemList.titreFormAdd = "d'un module";
            $scope.itemList.titreFormUpdate = 'du module';
            $scope.itemList.champs = [
                {
                    nom: 'Nom *',
                    field: 'nom',
                    type: 'input_text',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Marge *',
                    field: 'marge',
                    type: 'input_float',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Description *',
                    field: 'description',
                    type: 'input_textarea',
                    parentDivClass: 'col-12'
                },
                {
                    nom: 'Gamme *',
                    field: 'id_gamme',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    options: GammeOptions
                },
                {
                    nom: 'Nature de module *',
                    field: 'id_nature',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    options: NatureModuleOptions
                },
                {
                    type: 'input_grid',
                    parentDivClass: 'col-12',
                    modelCible: 'composant',
                    cols: [
                        {
                            label: 'Nom',
                            champ: 'composant_nom'
                        },
                        {
                            label: 'Quantité',
                            champ: 'composant_quantite'
                        },
                        {
                            label: 'Prix unitaire',
                            champ: 'prix'
                        },
                        {
                            label: 'Marge',
                            champ: 'composant_marge'
                        }
                    ],
                    champsModal: [
                        {
                            nom: 'Nom *',
                            field: 'composant_nom',
                            type: 'input_select',
                            parentDivClass: 'col-12',
                            options: ComposantOptions
                        },
                        {
                            nom: 'Quantité *',
                            field: 'composant_quantite',
                            type: 'input_number',
                            parentDivClass: 'col-12'
                        }
                    ]
                }
            ];

            angular.extend(this, $controller('formulairesCtrl', {$scope: $scope}));
        }]);
});
