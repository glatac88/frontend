define(['app', 'noty'], function (app, noty) {
    app.register.controller('profilCtrl', ['$rootScope', '$scope', '$location', '$timeout', function ($rootScope, $scope, $location, $timeout) {

        $scope.utilisateur= {};
        var nom = $scope.utilisateur.nom = "hfuizehf";
        var prenom = $scope.utilisateur.prenom = "hdzuhdaz";

        $scope.titre = nom + " " + prenom;
            $scope.updateUser = function () {
            var data = new FormData();

            //Récupération des paramètres
            data.append('nom', $scope.utilisateur.nom);
            data.append('prenom', $scope.utilisateur.prenom);
            data.append('tel', $scope.utilisateur.tel);
            data.append('email', $scope.utilisateur.email);
            data.append('mdp', $scope.utilisateur.mdp);
            data.append('conf_mdp', $scope.utilisateur.conf_mdp);
            data.append('role', $scope.utilisateur.role);

            //Enregistrement des modifications du profil
            //Modif

            axios.post('/utilisateur/profil/', data)
                .then(function (response) {
                    $timeout(function () {
                        noty.showNotif('Profil modifié', 'success');
                        $location.path("/profil/profil");
                        console.log(response);
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('Le Profil n\'a pas pu être modifié : ' + error.data.messages.join(' | '), 'error');
                    });
                });
        };

    }])
});
