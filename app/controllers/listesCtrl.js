define(['app','noty'], function (app,noty) {
    app.register.controller('listesCtrl', ['$rootScope', '$scope','$location','$timeout', function ($rootScope, $scope,$location,$timeout) {

        //Initialisation variables du scope
        $scope.filtre = "";
        $scope.order = "";
        $scope.loading = false;
        $scope.current = [];

        $scope.itemList.current = 0;

        index($scope.itemList.current);

        // Récupération de la liste
        function index(page = 0, filtre = null, order = null) {

            var data = new FormData();

            if (filtre != null && filtre !== '') data.append('query', filtre);
            if (order != null) data.append('order', order);

            data.append('offset', 8 * page);

            $scope.loading = true;

            axios.post('/' + $scope.itemList.modelNom + '/search', data)
                .then(function (response) {
                    $scope.$apply(function () {

                        $scope.loading = false;

                        if (response.data) {

                          if (response.data.data) $scope.itemList.response = response.data.data;
                          if (response.data.total) $scope.itemList.total = response.data.total;
                          if (response.data.total) $scope.itemList.pages = Math.ceil(response.data.total / 8);
                        }
                    });
                })
                .catch(function (error) {

                    $scope.$apply(function () {

                        $scope.loading = false;
                        $scope.error = error.message;
                    });
                });
        };

        $rootScope.refreshTable = function(page = 0, filtre = null, order = null) {

            if (order != null && order + ' DESC' === $scope.order) {

                order = null;

                $timeout(function () {

                    $scope.order = order;
                });

            } else if (order != null && order === $scope.order && !order.includes(' DESC')) {

                order = order + ' DESC';

                $timeout(function () {

                    $scope.order = order;
                });

            } else if (order != null) {

                $timeout(function () {

                    $scope.order = order;
                });

            } else if ($scope.order != null) {

                order = $scope.order;
            }

            if (page !== $scope.itemList.current) {

                $timeout(function () {

                    $scope.itemList.current = page;
                });
            }

            if (filtre !== $scope.filtre) {

                $timeout(function () {

                    $scope.filtre = filtre;
                });
            }

            index(page, filtre, order);
        };



        $scope.edit = function (id) {
            if (id != null)
                $location.path('/'+$scope.itemList.moduleNom+'/'+$scope.itemList.dossierNom+'/formulaire/'+id);
            else
                noty.showNotif('L\'élément demandé n\'existe pas', 'error');
        };

        $scope.openModal = function (id,label) {
            $('.modal').modal('show');
            $scope.current.id = id;
            $scope.current.label = label;
        };

        $scope.delete = function (id) {
            axios.delete('/' + $scope.itemList.modelNom + '/' + id)
                .then(function () {
                    $timeout(function () {
                        noty.showNotif('Élément archivé', 'success');
                        index();
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément n\'a pas pu être archivé' + error, 'error');
                    });
                });
        };

    }])
});
