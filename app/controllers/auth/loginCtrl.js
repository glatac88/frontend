'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

define(['app', 'noty'], function (app, noty) {

    app.register.controller('loginCtrl', ['$rootScope', '$scope', '$timeout', '$location', function ($rootScope, $scope, $timeout, $location) {

        localStorage.clear();
        $scope.authLoading = false;

        // Event de connexion
        $scope.login = function () {

            var data = new FormData();
            data.append('email', $scope.username);
            data.append('mdp', $scope.password);

            axios.defaults.headers.common = {};

            $scope.authLoading = true;

            if ($scope.username && $scope.password) {

                // noinspection AmdModulesDependencies
                axios.post('/auth/login', data)
                    .then(function (response) {

                        $scope.authLoading = false;
                        localStorage.setItem("token", response.data.data["token"]);
                        localStorage.setItem("refreshToken", response.data.data["refreshToken"]);
                        axios.defaults.headers.common = {'Authorization': "Bearer " + response.data.data["token"]};

                        $timeout(function () {

                            noty.showNotif('Vous êtes maintenant connecté !', 'info');
                            $location.path('/');
                        });
                    })
                    .catch(function (error) {

                        $scope.authLoading = false;
                        noty.showError(error.data.message);
                    });

            } else {

                $scope.authLoading = false;
            }
        };
    }])
});
