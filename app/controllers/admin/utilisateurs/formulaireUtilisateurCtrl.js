'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

// noinspection JSUnusedLocalSymbols
define(['app', 'formulairesCtrl'], function (app, formulairesCtrl) {

    /**
     * @memberof app
     * @ngdoc controllers
     * @name formulaireUtilisateurCtrl
     */
    app.register.controller('formulaireUtilisateurCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            $scope.itemList = [];
            $scope.itemList.modelNom = 'utilisateur';
            $scope.itemList.moduleNom = 'admin';
            $scope.itemList.dossierNom = 'utilisateurs';
            $scope.itemList.titreFormAdd = "d'un utilisateur";
            $scope.itemList.titreFormUpdate = "de l'utilisateur";
            $scope.itemList.champs = [
                {
                    nom: 'Nom *',
                    field: 'nom',
                    type: 'input_text',
                    parentDivClass: 'col-6',
                    params: {
                        min: 0
                    }
                },
                {
                    nom: 'Prénom *',
                    field: 'prenom',
                    type: 'input_text',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Téléphone',
                    field: 'tel',
                    type: 'input_tel',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'E-mail *',
                    field: 'email',
                    type: 'input_mail',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Mot de passe *',
                    field: 'mdp',
                    type: 'input_mdp',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Confirmation mot de passe *',
                    field: 'conf_mdp',
                    type: 'input_mdp',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Rôle *',
                    field: 'role',
                    type: 'input_select',
                    parentDivClass: 'col-12'
                },
            ];

            angular.extend(this, $controller('formulairesCtrl', {$scope: $scope}));
        }]);
});
