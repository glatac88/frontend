'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

define(['config'], function (config) {

    // Stoppe l'application si le fichier de configuration n'existe pas
    if (typeof config === "undefined") {

        throw new Error("App : Le fichier de configuration n'existe pas");
    }

    // Démarre l'application AngularJS
    var app = angular.module('webapp', ['ngRoute']);

    app.filter('debug', function() {
        return function(input) {
            if (input === '') return 'empty string';
            return input ? input : ('' + input);
        };
    });

    // Configuration du module principal : app
    app.config(['$routeProvider', '$controllerProvider', '$provide', '$locationProvider',
        function ($routeProvider, $controllerProvider, $provide, $locationProvider) {

            $locationProvider.html5Mode(true);

            // noinspection JSUndefinedPropertyAssignment
            app.register = {
                controller: $controllerProvider.register,
                factory: $provide.factory
            };

            function resolveController(names) {
                return {
                    load: ['$q', '$rootScope', '$location', '$route', function ($q, $rootScope, $location, $route) {

                        var token = localStorage.getItem('token');
                        var refreshToken = localStorage.getItem('refreshToken');

                        if (_.isNull(token) || _.isNull(refreshToken)) {

                            localStorage.clear();
                            $rootScope.isAuth = false;
                            $location.path('/login');

                        } else {

                            // axios.get('/ping');

                            $rootScope.isAuth = (!_.isNull(token) || !_.isNull(refreshToken));
                            $rootScope.userSession = jwt_decode(token);

                            $rootScope.currentRoute = $route.current;
                        }

                        var defer = $q.defer();

                        require(names, function () {

                            defer.resolve();
                            $rootScope.$apply();
                        });

                        return defer.promise;
                    }]
                }
            }

            $routeProvider
                //Error
                .when('/404', {
                    templateUrl: 'app/views/errors/index.html'
                })

                //Home
                .when('/', {
                    templateUrl: 'app/views/home.html',
                    controller: 'homeCtrl',
                    resolve: resolveController(['homeCtrl'])
                })

                //Settings
                .when('/settings', {
                    redirectTo: '/settings/composants/liste-composants'
                })
                .when('/settings/composants/liste-composants', {
                    templateUrl: 'app/views/settings/composants/listeComposants.html',
                    controller: 'listeComposantsCtrl',
                    resolve: resolveController(['listeComposantsCtrl'])
                })
                .when('/settings/composants/formulaire/:id?', {
                    templateUrl: 'app/views/settings/composants/formulaireComposant.html',
                    controller: 'formulaireComposantCtrl',
                    resolve: resolveController(['formulaireComposantCtrl'])
                })
                .when('/settings/gammes/liste-gammes', {
                    templateUrl: 'app/views/settings/gammes/listeGammes.html',
                    controller: 'listeGammesCtrl',
                    resolve: resolveController(['listeGammesCtrl'])
                })
                .when('/settings/gammes/formulaire/:id?', {
                    templateUrl: 'app/views/settings/gammes/formulaireGamme.html',
                    controller: 'formulaireGammeCtrl',
                    resolve: resolveController(['formulaireGammeCtrl'])
                })
                .when('/settings/modules/liste-modules', {
                    templateUrl: 'app/views/settings/modules/listeModules.html',
                    controller: 'listeModulesCtrl',
                    resolve: resolveController(['listeModulesCtrl'])
                })
                .when('/settings/modules/formulaire/:id?', {
                    templateUrl: 'app/views/settings/modules/formulaireModule.html',
                    controller: 'formulaireModuleCtrl',
                    resolve: resolveController(['formulaireModuleCtrl'])
                })
                .when('/settings/tva/liste-tva', {
                    templateUrl: 'app/views/settings/tva/listeTva.html',
                    controller: 'listeTvaCtrl',
                    resolve: resolveController(['listeTvaCtrl'])
                })
                .when('/settings/tva/formulaire/:id?', {
                    templateUrl: 'app/views/settings/tva/formulaireTva.html',
                    controller: 'formulaireTvaCtrl',
                    resolve: resolveController(['formulaireTvaCtrl'])
                })
                .when('/settings/famillecomposants/liste-famillecomposants', {
                    templateUrl: 'app/views/settings/famillecomposants/listeFamilleComposants.html',
                    controller: 'listeFamilleComposantsCtrl',
                    resolve: resolveController(['listeFamilleComposantsCtrl'])
                })
                .when('/settings/famillecomposants/formulaire/:id?', {
                    templateUrl: 'app/views/settings/famillecomposants/formulaireFamilleComposant.html',
                    controller: 'formulaireFamilleComposantCtrl',
                    resolve: resolveController(['formulaireFamilleComposantCtrl'])
                })
                .when('/settings/typegammes/liste-typegammes', {
                    templateUrl: 'app/views/settings/typegammes/listeTypeGammes.html',
                    controller: 'listeTypeGammesCtrl',
                    resolve: resolveController(['listeTypeGammesCtrl'])
                })
                .when('/settings/typegammes/formulaire/:id?', {
                    templateUrl: 'app/views/settings/typegammes/formulaireTypeGamme.html',
                    controller: 'formulaireTypeGammeCtrl',
                    resolve: resolveController(['formulaireTypeGammeCtrl'])
                })
                .when('/settings/typefinitions/liste-typefinitions', {
                    templateUrl: 'app/views/settings/typefinitions/listeTypeFinitions.html',
                    controller: 'listeTypeFinitionsCtrl',
                    resolve: resolveController(['listeTypeFinitionsCtrl'])
                })
                .when('/settings/typefinitions/formulaire/:id?', {
                    templateUrl: 'app/views/settings/typefinitions/formulaireTypeFinition.html',
                    controller: 'formulaireTypeFinitionCtrl',
                    resolve: resolveController(['formulaireTypeFinitionCtrl'])
                })
                .when('/settings/typeisolants/liste-typeisolants', {
                    templateUrl: 'app/views/settings/typeisolants/listeTypeIsolants.html',
                    controller: 'listeTypeIsolantsCtrl',
                    resolve: resolveController(['listeTypeIsolantsCtrl'])
                })
                .when('/settings/typeisolants/formulaire/:id?', {
                    templateUrl: 'app/views/settings/typeisolants/formulaireTypeIsolant.html',
                    controller: 'formulaireTypeIsolantCtrl',
                    resolve: resolveController(['formulaireTypeIsolantCtrl'])
                })
                .when('/settings/naturemodules/liste-naturemodules', {
                    templateUrl: 'app/views/settings/naturemodules/listeNatureModules.html',
                    controller: 'listeNatureModulesCtrl',
                    resolve: resolveController(['listeNatureModulesCtrl'])
                })
                .when('/settings/naturemodules/formulaire/:id?', {
                    templateUrl: 'app/views/settings/naturemodules/formulaireNatureModule.html',
                    controller: 'formulaireNatureModuleCtrl',
                    resolve: resolveController(['formulaireNatureModuleCtrl'])
                })
                .when('/settings/etatdevis/liste-etatdevis', {
                    templateUrl: 'app/views/settings/etatdevis/listeEtatDevis.html',
                    controller: 'listeEtatDevisCtrl',
                    resolve: resolveController(['listeEtatDevisCtrl'])
                })
                .when('/settings/etatdevis/formulaire/:id?', {
                    templateUrl: 'app/views/settings/etatdevis/formulaireEtatDevis.html',
                    controller: 'formulaireEtatDevisCtrl',
                    resolve: resolveController(['formulaireEtatDevisCtrl'])
                })

                //Gestion
                .when('/gestion', {
                    redirectTo: '/gestion/fournisseurs/liste-fournisseurs'
                })
                .when('/gestion/fournisseurs/liste-fournisseurs', {
                    templateUrl: 'app/views/gestion/fournisseurs/listeFournisseurs.html',
                    controller: 'listeFournisseursCtrl',
                    resolve: resolveController(['listeFournisseursCtrl'])
                })
                .when('/gestion/fournisseurs/formulaire/:id?', {
                    templateUrl: 'app/views/gestion/fournisseurs/formulaireFournisseur.html',
                    controller: 'formulaireFournisseurCtrl',
                    resolve: resolveController(['formulaireFournisseurCtrl'])
                })
                .when('/gestion/clients/liste-clients', {
                    templateUrl: 'app/views/gestion/clients/listeClients.html',
                    controller: 'listeClientsCtrl',
                    resolve: resolveController(['listeClientsCtrl'])
                })
                .when('/gestion/clients/formulaire/:id?', {
                    templateUrl: 'app/views/gestion/clients/formulaireClient.html',
                    controller: 'formulaireClientCtrl',
                    resolve: resolveController(['formulaireClientCtrl'])
                })

                //Admin
                .when('/admin', {
                    redirectTo: '/admin/utilisateurs/liste-utilisateurs'
                })
                .when('/admin/utilisateurs/liste-utilisateurs', {
                    templateUrl: 'app/views/admin/utilisateurs/listeUtilisateurs.html',
                    controller: 'listeUtilisateursCtrl',
                    resolve: resolveController(['listeUtilisateursCtrl'])
                })
                .when('/admin/utilisateurs/formulaire/:id?', {
                    templateUrl: 'app/views/admin/utilisateurs/formulaireUtilisateur.html',
                    controller: 'formulaireUtilisateurCtrl',
                    resolve: resolveController(['formulaireUtilisateurCtrl'])
                })

                //Login
                .when('/login', {
                    templateUrl: 'app/views/auth/login.html',
                    controller: 'loginCtrl',
                    resolve: resolveController(['loginCtrl'])
                })
                .when('/logout', {
                    templateUrl: 'app/views/auth/login.html',
                    controller: 'logoutCtrl',
                    resolve: resolveController(['logoutCtrl'])
                })

                //Conception
                .when('/conception', {
                    redirectTo: '/conception/projets/liste-projets'
                })
                .when('/conception/devis/liste-devis', {
                    templateUrl: 'app/views/conception/devis/listeDevis.html',
                    controller: 'listeDevisCtrl',
                    resolve: resolveController(['listeDevisCtrl'])
                })
                .when('/conception/devis/formulaire/:id?', {
                    templateUrl: 'app/views/conception/devis/formulaireDevis.html',
                    controller: 'formulaireDevisCtrl',
                    resolve: resolveController(['formulaireDevisCtrl'])
                })
                .when('/conception/projets/liste-projets', {
                    templateUrl: 'app/views/conception/projets/listeProjets.html',
                    controller: 'listeProjetsCtrl',
                    resolve: resolveController(['listeProjetsCtrl'])
                })
                .when('/conception/projets/formulaire/:id?', {
                    templateUrl: 'app/views/conception/projets/formulaireProjet.html',
                    controller: 'formulaireProjetCtrl',
                    resolve: resolveController(['formulaireProjetCtrl'])
                })

                // Profil
                .when('/profil', {
                    templateUrl: 'app/views/profil/profil.html',
                    controller: 'profilCtrl',
                    resolve: resolveController(['profilCtrl'])
                })
                .otherwise({
                    redirectTo: '/'
                });
        }
    ]);

    // Lancement du module principal : app
    app.run(function ($rootScope, $timeout, $location) {

        // var socket = io.connect('ws://madera.lxd:2021');
        //
        // socket.emit('subscriber', 'refresh');
        // socket.emit('subscriber', 'chat');
        //
        // socket.on('refresh', function (data) {
        //     console.log(data);
        //     socket.emit('chat', { my: 'data' });
        // });

        axios.defaults.baseURL = config.Api.baseURL;
        axios.defaults.headers.common = {'Authorization': "Bearer " + localStorage.getItem('token')};

        axios.interceptors.response.use(function (response) {

            return response;

        }, function(error) {

            var originalRequest = error.config;

            if (error.response) {

                if (error.response.status === 401) {

                    var data = new FormData();
                    data.append('refreshToken', localStorage.getItem('refreshToken'));

                    return axios.post('/auth/regenerate/', data)
                        .then(function (response) {

                            localStorage.setItem('token', response.data.data.token);

                            if (response.data.data.refresh) {

                                // noinspection JSUnresolvedVariable
                                localStorage.setItem('refreshToken', response.data.data.refreshToken);
                            }

                            axios.defaults.headers.common = {'Authorization': "Bearer " + response.data.data.token};
                            originalRequest.headers['Authorization'] = 'Bearer ' + response.data.data.token;

                            return axios(originalRequest);
                        })
                        .catch(function () {

                            $timeout(function () {

                                localStorage.clear();
                                $rootScope.isAuth = false;
                                $location.path('/login');
                            });
                        });
                }
            }

            return Promise.reject(error);
        });
    });

    // Bootstrap l'application AngularJS
    // noinspection JSCheckFunctionSignatures
    angular.element(document).ready(function () {

        // noinspection JSCheckFunctionSignatures
        angular.bootstrap(document, ['webapp']);
    });

    return app;
});
