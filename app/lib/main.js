'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

requirejs.config({
    baseUrl: 'app',
    paths: {
        // Libs
        'config': 'cfg/config',
        'app': 'lib/app',

        // Services
        'noty': 'services/notySrv',
        'global':'services/globalSrv',

        // Home
        'homeCtrl': 'controllers/homeCtrl',

        // Liste & formulaire
        'listesCtrl': 'controllers/listesCtrl',
        'formulairesCtrl': 'controllers/formulairesCtrl',

        // Auth
        'loginCtrl': 'controllers/auth/loginCtrl',
        'logoutCtrl': 'controllers/auth/logoutCtrl',

        // Settings
        'listeComposantsCtrl': 'controllers/settings/composants/listeComposantsCtrl',
        'formulaireComposantCtrl': 'controllers/settings/composants/formulaireComposantCtrl',

        'listeGammesCtrl': 'controllers/settings/gammes/listeGammesCtrl',
        'formulaireGammeCtrl': 'controllers/settings/gammes/formulaireGammeCtrl',

        'listeModulesCtrl': 'controllers/settings/modules/listeModulesCtrl',
        'formulaireModuleCtrl': 'controllers/settings/modules/formulaireModuleCtrl',

        'listeTvaCtrl': 'controllers/settings/tva/listeTvaCtrl',
        'formulaireTvaCtrl': 'controllers/settings/tva/formulaireTvaCtrl',

        'listeFamilleComposantsCtrl': 'controllers/settings/famillecomposants/listeFamilleComposantsCtrl',
        'formulaireFamilleComposantCtrl': 'controllers/settings/famillecomposants/formulaireFamilleComposantCtrl',

        'listeTypeGammesCtrl': 'controllers/settings/typegammes/listeTypeGammesCtrl',
        'formulaireTypeGammeCtrl': 'controllers/settings/typegammes/formulaireTypeGammeCtrl',

        'listeTypeFinitionsCtrl': 'controllers/settings/typefinitions/listeTypeFinitionsCtrl',
        'formulaireTypeFinitionCtrl': 'controllers/settings/typefinitions/formulaireTypeFinitionCtrl',

        'listeNatureModulesCtrl': 'controllers/settings/naturemodules/listeNatureModulesCtrl',
        'formulaireNatureModuleCtrl': 'controllers/settings/naturemodules/formulaireNatureModuleCtrl',

        'listeTypeIsolantsCtrl': 'controllers/settings/typeisolants/listeTypeIsolantsCtrl',
        'formulaireTypeIsolantCtrl': 'controllers/settings/typeisolants/formulaireTypeIsolantCtrl',

        'listeEtatDevisCtrl': 'controllers/settings/etatdevis/listeEtatDevisCtrl',
        'formulaireEtatDevisCtrl': 'controllers/settings/etatdevis/formulaireEtatDevisCtrl',

        //Gestion
        'listeFournisseursCtrl': 'controllers/gestion/fournisseurs/listeFournisseursCtrl',
        'formulaireFournisseurCtrl': 'controllers/gestion/fournisseurs/formulaireFournisseurCtrl',

        'listeClientsCtrl': 'controllers/gestion/clients/listeClientsCtrl',
        'formulaireClientCtrl': 'controllers/gestion/clients/formulaireClientCtrl',

        //Admin
        'listeUtilisateursCtrl': 'controllers/admin/utilisateurs/listeUtilisateursCtrl',
        'formulaireUtilisateurCtrl': 'controllers/admin/utilisateurs/formulaireUtilisateurCtrl',

        // Devis
        'listeDevisCtrl': 'controllers/conception/devis/listeDevisCtrl',
        'formulaireDevisCtrl': 'controllers/conception/devis/formulaireDevisCtrl',

        // Projets
        'listeProjetsCtrl': 'controllers/conception/projets/listeProjetsCtrl',
        'formulaireProjetCtrl': 'controllers/conception/projets/formulaireProjetCtrl',

        // Profil
        'profilCtrl': 'controllers/profil/profilCtrl',

        // Errors
        'error404Ctrl': 'controllers/error/error404Ctrl'
    },
    deps: ['app']
});
