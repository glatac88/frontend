define(function () {

    return {
        Api: {
            // baseURL: 'http://api.madera.lxd/'
            baseURL: 'https://apimadera.toroia.fr/'
        },
        Noty: {
            theme : "metroui",
            layout : "topRight",
            timeout : 5000,
            progressBar : true,
        }
    };
});
